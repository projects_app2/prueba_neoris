package com.prueba.repository;

import com.prueba.entity.Movimientos;
import com.prueba.model.Reporte;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovimientosRepository extends JpaRepository<Movimientos, Long> {


    @Query( value= " select count(*) as numero from Movimientos m join Cuenta c on m.idCuenta = c.idCuenta " +
            " where c.numero = :numero  "
            , nativeQuery=true )
    Integer getNumeroMovimientosByNumeroCuenta(@Param("numero") String numero);

    @Query( value= " select m.saldo from Movimientos m join Cuenta c on m.idCuenta = c.idCuenta " +
            " where c.numero = :numero and m.idMovimientos = ( SELECT MAX(idMovimientos) FROM Movimientos ) "
            , nativeQuery=true )
    Double getSaldoActualMovimientosByNumeroCuenta(@Param("numero") String numero);

    @Query( value = " select m.fecha,cc.nombre as cliente,c.numero as numeroCuenta,c.tipo,c.saldo as saldoInicial,c.estado,m.valor as movimiento,m.saldo as saldoDisponible  " +
            " from Movimientos m join Cuenta c on m.idCuenta = c.idCuenta " +
            " join cliente cc on c.idCliente = cc.idCliente " +
            " where c.numero = :numero and DATE_FORMAT(m.fecha,'%d/%m/%Y') BETWEEN :fechaInicio AND :fechaFin "
            , nativeQuery=true)
    List<Object[]> getReporte(@Param("numero") String numero, @Param("fechaInicio") String fechaInicio ,@Param("fechaFin") String fechaFin);

}
