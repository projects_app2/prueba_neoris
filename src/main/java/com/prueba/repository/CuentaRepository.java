package com.prueba.repository;

import com.prueba.entity.Cuenta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CuentaRepository extends JpaRepository<Cuenta, Long> {

    Cuenta findCuentaByNumero(String numeroCuenta);

}
