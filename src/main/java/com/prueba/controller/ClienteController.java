package com.prueba.controller;

import com.prueba.entity.Cliente;
import com.prueba.exception.BusinessException;
import com.prueba.model.ResponseData;
import com.prueba.model.RespuestaError;
import com.prueba.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/prueba")
@CrossOrigin(origins = "*")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @GetMapping("/clientes")
    public ResponseEntity<?> getClientes(){

        ResponseData<Object> response = new ResponseData<Object>();

        try{

            List<Cliente> listClientes = clienteService.getAllClientePersona();
            if (listClientes != null && !listClientes.isEmpty()) {
                response.setSuccess(true);
                response.setNotificaciones(new ArrayList<>());
                response.setData(listClientes);
                response.setMessage("Registros encontrados.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }else {
                response.setSuccess(true);
                response.setData(new ArrayList<>());
                response.setNotificaciones(new ArrayList<>());
                response.setMessage("Registros vacios.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }

        }catch (BusinessException be) {
            int numberHTTPDesired = Integer.parseInt(be.getRespuestaError().getCodigo());
            RespuestaError respuestaError = be.getRespuestaError();
            response.setSuccess(false);
            response.setNotificaciones(respuestaError);
            response.setData(new ArrayList<>());
            response.setMessage("Ocurrio un error.");
            return new ResponseEntity<>(response, HttpStatus.valueOf(numberHTTPDesired));
        }



    }

    @GetMapping("/clientes/{id}")
    public ResponseEntity<?> getClienteById(@PathVariable("id") Long idCliente){

        ResponseData<Object> response = new ResponseData<Object>();


        try {
            Cliente cliente = clienteService.getByIdCliente(idCliente);

            if (cliente != null ) {
                response.setSuccess(true);
                response.setNotificaciones(new ArrayList<>());
                response.setData(cliente);
                response.setMessage("Cliente encontrado.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }else {
                response.setSuccess(true);
                response.setData(new ArrayList<>());
                response.setNotificaciones(new ArrayList<>());
                response.setMessage("Cliente no encontrado.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (BusinessException be) {
            int numberHTTPDesired = Integer.parseInt(be.getRespuestaError().getCodigo());
            RespuestaError respuestaError = be.getRespuestaError();
            response.setSuccess(false);
            response.setNotificaciones(respuestaError);
            response.setData(new ArrayList<>());
            response.setMessage("Ocurrio un error.");
            return new ResponseEntity<>(response, HttpStatus.valueOf(numberHTTPDesired));
        }


    }

    @PostMapping ("/clientes")
    public ResponseEntity<?> postClientes(@RequestBody Cliente request){
        ResponseData<Object> response = new ResponseData<Object>();

        try{

            Cliente clienteResponse = clienteService.saveClientePersona(request);

            if(clienteResponse != null){
                response.setSuccess(true);
                response.setNotificaciones(new ArrayList<>());
                response.setData(clienteResponse);
                response.setMessage("Cliente registrado.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }else{
                response.setSuccess(true);
                response.setNotificaciones(new ArrayList<>());
                response.setData(request);
                response.setMessage("Cliente no registrado.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }

        }catch (BusinessException be) {
            int numberHTTPDesired = Integer.parseInt(be.getRespuestaError().getCodigo());
            RespuestaError respuestaError = be.getRespuestaError();
            response.setSuccess(false);
            response.setNotificaciones(respuestaError);
            response.setData(new ArrayList<>());
            response.setMessage("Ocurrio un error.");
            return new ResponseEntity<>(response, HttpStatus.valueOf(numberHTTPDesired));
        }


    }

    @PutMapping ("/clientes")
    public ResponseEntity<?> putClientes(@RequestBody Cliente request){

        ResponseData<Object> response = new ResponseData<Object>();

        try{

            if(request != null && request.getIdCliente() != null){
                Cliente clienteResponse = clienteService.saveClientePersona(request);

                if(clienteResponse != null){
                    response.setSuccess(true);
                    response.setNotificaciones(new ArrayList<>());
                    response.setData(clienteResponse);
                    response.setMessage("Cliente actualizado.");
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }else{
                    response.setSuccess(true);
                    response.setNotificaciones(new ArrayList<>());
                    response.setData(request);
                    response.setMessage("Cliente no actualizado.");
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }

            }else{
                response.setSuccess(true);
                response.setNotificaciones(new ArrayList<>());
                response.setMessage("El id de cliente no presente.");
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }

        }catch (BusinessException be) {
            int numberHTTPDesired = Integer.parseInt(be.getRespuestaError().getCodigo());
            RespuestaError respuestaError = be.getRespuestaError();
            response.setSuccess(false);
            response.setNotificaciones(respuestaError);
            response.setData(new ArrayList<>());
            response.setMessage("Ocurrio un error.");
            return new ResponseEntity<>(response, HttpStatus.valueOf(numberHTTPDesired));
        }
    }

    @DeleteMapping("/clientes/{id}")
    public ResponseEntity<?> deleteClienteById(@PathVariable("id") Long idCliente){
        ResponseData<Object> response = new ResponseData<Object>();

        try {
            clienteService.deleteClientePersona(idCliente);
            response.setSuccess(true);
            response.setData(new ArrayList<>());
            response.setNotificaciones(new ArrayList<>());
            response.setMessage("Cliente eliminado.");
            return new ResponseEntity<>(response, HttpStatus.OK);

        }catch (BusinessException be) {
            int numberHTTPDesired = Integer.parseInt(be.getRespuestaError().getCodigo());
            RespuestaError respuestaError = be.getRespuestaError();
            response.setSuccess(false);
            response.setNotificaciones(respuestaError);
            response.setData(new ArrayList<>());
            response.setMessage("Ocurrio un error.");
            return new ResponseEntity<>(response, HttpStatus.valueOf(numberHTTPDesired));
        }
    }

}
