package com.prueba.controller;

import com.prueba.entity.Movimientos;
import com.prueba.exception.BusinessException;
import com.prueba.model.ResponseData;
import com.prueba.model.RespuestaError;
import com.prueba.service.MovimientoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/prueba")
@CrossOrigin(origins = "*")
public class MovimientoController {

    @Autowired
    MovimientoService movimientoService;


    @GetMapping("/movimientos")
    public ResponseEntity<?> getCuentas(){

        ResponseData<Object> response = new ResponseData<Object>();

        try{
            List<Movimientos> listMovimientos = movimientoService.getAllMovimientos();
            if (listMovimientos != null && !listMovimientos.isEmpty()) {
                response.setSuccess(true);
                response.setNotificaciones(new ArrayList<>());
                response.setData(listMovimientos);
                response.setMessage("Registros encontrados.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }else {
                response.setSuccess(true);
                response.setData(new ArrayList<>());
                response.setNotificaciones(new ArrayList<>());
                response.setMessage("Registros vacios.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (BusinessException be) {
            int numberHTTPDesired = Integer.parseInt(be.getRespuestaError().getCodigo());
            RespuestaError respuestaError = be.getRespuestaError();
            response.setSuccess(false);
            response.setNotificaciones(respuestaError);
            response.setData(new ArrayList<>());
            response.setMessage("Ocurrio un error.");
            return new ResponseEntity<>(response, HttpStatus.valueOf(numberHTTPDesired));
        }



    }

    @GetMapping("/movimientos/{id}")
    public ResponseEntity<?> getCuentaById(@PathVariable("id") Long idMovimiento){

        ResponseData<Object> response = new ResponseData<Object>();

        try {
            Movimientos movimiento = movimientoService.getByIdMovimiento(idMovimiento);

            if (movimiento != null ) {
                response.setSuccess(true);
                response.setNotificaciones(new ArrayList<>());
                response.setData(movimiento);
                response.setMessage("Movimiento encontrado.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }else {
                response.setSuccess(true);
                response.setData(new ArrayList<>());
                response.setNotificaciones(new ArrayList<>());
                response.setMessage("Movimiento no encontrado.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        }catch (BusinessException be) {
            int numberHTTPDesired = Integer.parseInt(be.getRespuestaError().getCodigo());
            RespuestaError respuestaError = be.getRespuestaError();
            response.setSuccess(false);
            response.setNotificaciones(respuestaError);
            response.setData(new ArrayList<>());
            response.setMessage("Ocurrio un error.");
            return new ResponseEntity<>(response, HttpStatus.valueOf(numberHTTPDesired));
        }


    }

    @PostMapping ("/movimientos")
    public ResponseEntity<?> postCuenta(@RequestBody Movimientos request){
        ResponseData<Object> response = new ResponseData<Object>();

        try{

            Movimientos movimientoResponse = movimientoService.saveMovimiento(request);

            if(movimientoResponse != null){
                response.setNotificaciones(new ArrayList<>());
                response.setData(movimientoResponse);
                response.setMessage("Movimiento registrado.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }else{
                response.setNotificaciones(new ArrayList<>());
                response.setData(request);
                response.setMessage("Movimiento no registrado.");
                return new ResponseEntity<>(response, HttpStatus.OK);
            }

        }catch (BusinessException be ){
            int numberHTTPDesired = Integer.parseInt(be.getRespuestaError().getCodigo());
            RespuestaError respuestaError = be.getRespuestaError();
            response.setSuccess(false);
            response.setNotificaciones(respuestaError);
            response.setData(new ArrayList<>());
            response.setMessage("Ocurrio un error.");
            return new ResponseEntity<>(response, HttpStatus.valueOf(numberHTTPDesired));
        }


    }

    @PutMapping ("/movimientos")
    public ResponseEntity<?> putCuenta(@RequestBody Movimientos request){

        ResponseData<Object> response = new ResponseData<Object>();

        try{
            if(request != null && request.getIdMovimientos() != null){
                Movimientos movimientoResponse = movimientoService.updateMovimiento(request);

                if(movimientoResponse != null){
                    response.setSuccess(true);
                    response.setNotificaciones(new ArrayList<>());
                    response.setData(movimientoResponse);
                    response.setMessage("Movimiento actualizado.");
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }else{
                    response.setSuccess(true);
                    response.setNotificaciones(new ArrayList<>());
                    response.setData(request);
                    response.setMessage("Movimiento no actualizado.");
                    return new ResponseEntity<>(response, HttpStatus.OK);
                }

            }else{
                response.setSuccess(true);
                response.setNotificaciones(new ArrayList<>());
                response.setMessage("El id del movimiento no presente.");
                return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
            }
        }catch (BusinessException be ){
            int numberHTTPDesired = Integer.parseInt(be.getRespuestaError().getCodigo());
            RespuestaError respuestaError = be.getRespuestaError();
            response.setSuccess(false);
            response.setNotificaciones(respuestaError);
            response.setData(new ArrayList<>());
            response.setMessage("Ocurrio un error.");
            return new ResponseEntity<>(response, HttpStatus.valueOf(numberHTTPDesired));
        }
    }

    @DeleteMapping("/movimientos/{id}")
    public ResponseEntity<?> deleteMovimientoById(@PathVariable("id") Long idMovimiento){
        ResponseData<Object> response = new ResponseData<Object>();

        try {
            movimientoService.deleteMovimiento(idMovimiento);
            response.setSuccess(true);
            response.setData(new ArrayList<>());
            response.setNotificaciones(new ArrayList<>());
            response.setMessage("Movimiento eliminado.");
            return new ResponseEntity<>(response, HttpStatus.OK);

        }catch (BusinessException be) {
            int numberHTTPDesired = Integer.parseInt(be.getRespuestaError().getCodigo());
            RespuestaError respuestaError = be.getRespuestaError();
            response.setSuccess(false);
            response.setNotificaciones(respuestaError);
            response.setData(new ArrayList<>());
            response.setMessage("Ocurrio un error.");
            return new ResponseEntity<>(response, HttpStatus.valueOf(numberHTTPDesired));
        }
    }



}
