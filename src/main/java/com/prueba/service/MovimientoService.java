package com.prueba.service;

import com.prueba.entity.Cuenta;
import com.prueba.entity.Movimientos;
import com.prueba.exception.BusinessException;
import com.prueba.model.Reporte;

import java.util.List;

public interface MovimientoService {

    Movimientos saveMovimiento(Movimientos movimientos) throws BusinessException;

    List<Movimientos> getAllMovimientos() throws BusinessException;

    Movimientos getByIdMovimiento(Long idMovimiento) throws BusinessException;

    Movimientos updateMovimiento(Movimientos movimientos) throws BusinessException;

    void deleteMovimiento(Long idMovimiento) throws BusinessException;

    List<Reporte> getReporte(String fechaInicio,String fechafin,String numeroCuenta) throws BusinessException;

}
