package com.prueba.service;

import com.prueba.entity.Cliente;
import com.prueba.entity.Cuenta;
import com.prueba.exception.BusinessException;

import java.util.List;

public interface CuentaService {

    Cuenta saveCuenta(Cuenta cuenta) throws BusinessException;

    List<Cuenta> getAllCluentas() throws BusinessException;

    Cuenta getByIdCuenta(Long idCuenta) throws BusinessException;

    Cuenta updateCuenta(Cuenta cuenta) throws BusinessException;

    Cuenta getCuentaByNumero(String numeroCuenta) throws BusinessException;

    void deleteCuenta(Long idCuenta) throws BusinessException;

}
