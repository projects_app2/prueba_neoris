package com.prueba.service.impl;

import com.prueba.entity.Cliente;
import com.prueba.exception.BusinessException;
import com.prueba.repository.ClienteRepository;
import com.prueba.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("clienteServiceImpl")
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    ClienteRepository clienteRepository;

    @Override
    public Cliente saveClientePersona(Cliente cliente) throws BusinessException  {
        try{
            return clienteRepository.save(cliente);
        }catch (Exception e){
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage(),"Error interno");
        }
    }

    @Override
    public List<Cliente> getAllClientePersona()  throws BusinessException  {
        try{
            return clienteRepository.findAll();
        }catch (Exception e){
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage(),"Error interno");
        }
    }

    @Override
    public Cliente getByIdCliente(Long idCliente) throws BusinessException {
        try{
            return clienteRepository.findById(idCliente).orElse(null);
        }catch (Exception e){
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage(),"Error interno");
        }
    }

    @Override
    public Cliente updateClientePersona(Cliente cliente) throws BusinessException  {
        try {
            return clienteRepository.save(cliente);
        }catch (Exception e){
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage(),"Error interno");
        }
    }

    @Override
    public void deleteClientePersona(Long idCliente) throws BusinessException  {
        try{
            clienteRepository.deleteById(idCliente);
        }catch (Exception e){
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage(),"Error interno");
        }
    }
}
