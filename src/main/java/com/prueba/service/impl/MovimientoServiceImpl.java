package com.prueba.service.impl;

import com.prueba.entity.Cuenta;
import com.prueba.entity.Movimientos;
import com.prueba.exception.BusinessException;
import com.prueba.model.Reporte;
import com.prueba.repository.MovimientosRepository;
import com.prueba.service.CuentaService;
import com.prueba.service.MovimientoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("movimientoServiceImpl")
public class MovimientoServiceImpl implements MovimientoService {

    @Autowired
    MovimientosRepository movimientosRepository;

    @Autowired
    CuentaService cuentaService;

    @Override
    public Movimientos saveMovimiento(Movimientos movimientos) throws BusinessException {

        try{

            Cuenta cuenta =  cuentaService.getCuentaByNumero(movimientos.getNumeroCuenta());
            if(cuenta!= null){

                movimientos.setIdCuenta(cuenta);
                Integer numeroMovimientosByNumeroCuenta = movimientosRepository.getNumeroMovimientosByNumeroCuenta(movimientos.getNumeroCuenta());

                if(numeroMovimientosByNumeroCuenta == 0){

                    System.out.println("salgo caso 1 : " + cuenta.getSaldo());
                    if(cuenta.getSaldo() != null && cuenta.getSaldo() > 0){
                        Double saldo = cuenta.getSaldo() + ( movimientos.getValor() );
                        movimientos.setSaldo(saldo);
                        return movimientosRepository.save(movimientos);
                    }else{
                        throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,"Saldo no disponible.","Saldo no disponible." );
                    }


                }else{

                    movimientos.setSaldo( movimientosRepository.getSaldoActualMovimientosByNumeroCuenta(movimientos.getNumeroCuenta()) );
                    if(movimientos.getSaldo() != null && movimientos.getSaldo() > 0){
                        Double saldo = movimientos.getSaldo() + ( movimientos.getValor() );
                        movimientos.setSaldo(saldo);
                        return movimientosRepository.save(movimientos);
                    }else{
                        throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,"Saldo no disponible.","Saldo no disponible." );
                    }

                }

            }else{
                throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,"Cuenta no existe","Cuenta con el numero: " + movimientos.getNumeroCuenta() + " no existe." );
            }


        }catch (Exception e){
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage(),"Error interno");
        }

    }

    @Override
    public List<Movimientos> getAllMovimientos() throws BusinessException {
        try{
            return movimientosRepository.findAll();
        }catch (Exception e){
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage(),"Error interno");
        }
    }

    @Override
    public Movimientos getByIdMovimiento(Long idMovimiento) throws BusinessException {
        try {
            return movimientosRepository.findById(idMovimiento).orElse(null);
        }catch (Exception e){
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage(),"Error interno");
        }
    }

    @Override
    public Movimientos updateMovimiento(Movimientos movimientos) throws BusinessException {
        try{
            return movimientosRepository.save(movimientos);
        }catch (Exception e){
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage(),"Error interno");
        }
    }

    @Override
    public void deleteMovimiento(Long idMovimiento) throws BusinessException {
        try{
            movimientosRepository.deleteById(idMovimiento);
        }catch (Exception e){
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage(),"Error interno");
        }
    }

    @Override
    public List<Reporte> getReporte(String fechaInicio, String fechafin, String numeroCuenta) throws BusinessException {
        try{
            List<Object[]> res =  movimientosRepository.getReporte(numeroCuenta,fechaInicio,fechafin);

            List<Reporte> reporte = new ArrayList<>();
            for (Object[] reportDetail : res) {
                Reporte re = new Reporte();
                re.setFecha(reportDetail[0].toString());
                re.setCliente(reportDetail[1].toString());
                re.setNumeroCuenta(reportDetail[2].toString());
                re.setTipo(reportDetail[3].toString());
                re.setSaldoInicial(Double.valueOf(reportDetail[4].toString()));
                re.setEstado(Short.valueOf(reportDetail[5].toString()));
                re.setMovimiento(Double.valueOf(reportDetail[6].toString()));
                re.setSaldoDisponible(Double.valueOf(reportDetail[7].toString()));
                reporte.add(re);
            }
            return reporte;
        }catch (Exception e){
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage(),"Error interno");
        }
    }
}
