package com.prueba.service.impl;

import com.prueba.entity.Cuenta;
import com.prueba.exception.BusinessException;
import com.prueba.repository.CuentaRepository;
import com.prueba.service.CuentaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("cuentaServiceImpl")
public class CuentaServiceImpl implements CuentaService {

    @Autowired
    CuentaRepository cuentaRepository;

    @Override
    public Cuenta saveCuenta(Cuenta cuenta) throws BusinessException {
        try{
            return cuentaRepository.save(cuenta);
        }catch (Exception e){
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage(),"Error interno");
        }
    }

    @Override
    public List<Cuenta> getAllCluentas() throws BusinessException {
        try{
            return cuentaRepository.findAll();
        }catch (Exception e){
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage(),"Error interno");
        }
    }

    @Override
    public Cuenta getByIdCuenta(Long idCuenta) throws BusinessException {
        try{
            return cuentaRepository.findById(idCuenta).orElse(null);
        }catch (Exception e){
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage(),"Error interno");
        }
    }

    @Override
    public Cuenta updateCuenta(Cuenta cuenta) throws BusinessException {
        try{
            return cuentaRepository.save(cuenta);
        }catch (Exception e){
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage(),"Error interno");
        }
    }

    @Override
    public Cuenta getCuentaByNumero(String numeroCuenta) throws BusinessException {
        try{
            return cuentaRepository.findCuentaByNumero(numeroCuenta);
        }catch (Exception e){
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage(),"Error interno");
        }
    }

    @Override
    public void deleteCuenta(Long idCuenta)  throws BusinessException {

        try{
            cuentaRepository.deleteById(idCuenta);
        }catch (Exception e){
            throw new BusinessException(HttpStatus.INTERNAL_SERVER_ERROR,e.getMessage(),"Error interno");
        }
    }
}
