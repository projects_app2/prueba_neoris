package com.prueba.service;

import com.prueba.entity.Cliente;
import com.prueba.exception.BusinessException;

import java.util.List;

public interface ClienteService {

    Cliente saveClientePersona(Cliente cliente) throws BusinessException;

    List<Cliente> getAllClientePersona() throws BusinessException;

    Cliente getByIdCliente(Long idCliente) throws BusinessException;

    Cliente updateClientePersona(Cliente cliente) throws BusinessException;

    void deleteClientePersona(Long idCliente) throws BusinessException;

}
