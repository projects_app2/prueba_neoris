package com.prueba.model;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class RespuestaError {

    private String codigo;
    private String descripcion;
    private String mensajeerror;
    private String razonError;
    private String fecha;

    public RespuestaError(HttpStatus status, String businessMessage, String reasonPhrase) {
        this.codigo = String.valueOf(status.value());
        this.descripcion = status.getReasonPhrase();
        this.mensajeerror = businessMessage;
        this.razonError = reasonPhrase;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        this.fecha = dateFormat.format(new Date());
    }

}
