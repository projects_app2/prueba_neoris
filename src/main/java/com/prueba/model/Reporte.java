package com.prueba.model;

import lombok.Data;

@Data
public class Reporte {

    private String fecha;
    private String cliente;
    private String numeroCuenta;
    private String tipo;
    private Double saldoInicial;
    private Short estado;
    private Double movimiento;
    private Double saldoDisponible;

}
